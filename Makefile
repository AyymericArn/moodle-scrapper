target: requirements.txt

run:
	. venv/bin/activate
	python3 app/main.py
	deactivate

setup: requirements.txt
	if [ ! -d venv ]; then python3 -m venv venv; fi
	./venv/bin/pip install -r requirements.txt

clean:
	rm -rf __pycache__
	rm -rf venv

