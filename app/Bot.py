from discord import Intents
from discord.ext import commands

from Cookie import Cookie
from Parser import Parser

class Bot(commands.Bot):
    def __init__(self, command_prefix: str = "!", intents: Intents = Intents.default()):
        intents.message_content = True
        super().__init__(command_prefix=command_prefix, intents=intents, self_bot=False)
        self.add_commands()

    async def on_ready(self):
        print(f'{self.user} has connected to Discord!')

    def add_commands(self):
        @self.command()
        async def depots(ctx, *args):
            moodle_session = Cookie.moodle_session()
            depot_list = Parser.get_depots(moodle_session)
            if "-nr" in args:
                depot_list.remove_already_sent()
            await ctx.channel.send(f"```{depot_list}```")
