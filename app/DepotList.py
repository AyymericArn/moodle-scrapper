import re
from typing import *
from tabulate import tabulate
from os import path
import os
from Depot import Depot

class Color:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class DepotList:

    def __init__(self, depots: List[Depot]) -> None:
        self.depots = depots

    def __str__(self) -> str:
        return self.to_tab()

    def to_tab(self) -> str:
        lines = []
        for depot in self.depots:
            color = self.get_output_color(depot)
            cells = [
                color + depot.id + Color.ENDC,
                color + depot.course_title + Color.ENDC,
                color + depot.title + Color.ENDC,
                color + depot.status + Color.ENDC,
                color + depot.limit_date + Color.ENDC,
                color + depot.time_left + Color.ENDC
            ]
            lines.append(cells)
        return tabulate(lines, headers=["ID", "Cours", "Titre", "Status", "Date limite", "Temps restant"])

    def remove_already_sent(self) -> None:
        self.depots = [d for d in self.depots if "remis" != d.status.lower()]

    def remove_not_sent(self) -> None:
        self.depots = [d for d in self.depots if "remis" == d.status.lower()]

    def sort_by_time_left(self) -> None:
        for depot in self.depots:
            numbers = re.findall(r'\d+', depot.time_left)

            depot.days = 0
            depot.hours = 0
            if len(numbers) == 1:
                depot.hours = int(numbers[0])
            if len(numbers) == 2:
                depot.days = int(numbers[0])
                depot.hours = int(numbers[1])

            days_in_seconds = depot.days * 3600 * 24
            hours_in_seconds = depot.hours * 3600
            depot.time_left_in_seconds = days_in_seconds + hours_in_seconds
        self.depots.sort(key=lambda d: d.time_left_in_seconds, reverse=True)

    def get_output_color(self,depot : Depot):
        if "Non remis" in depot.status and "Retard" in depot.time_left:
            return Color.FAIL
        elif "Non remis" in depot.status and "Retard" not in depot.time_left:
            return Color.WARNING
        elif "Remis" in depot.status:
            return Color.OKGREEN
        else:
            return ""