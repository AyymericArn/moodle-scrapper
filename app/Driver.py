from decouple import config
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from time import sleep
from Mutex import Mutex

class Driver(webdriver.Remote):

    def __init__(self) -> None:
        chrome_options = Options()
        if config("HEADLESS") == "true":
            chrome_options.headless = True
        Driver.check_if_browser_used()
        super().__init__("http://selenium:4444/wd/hub", DesiredCapabilities.CHROME, options=chrome_options)
        Mutex.mark_browser_used()

    def check_if_browser_used():

        used = Mutex.browser_is_used()
        counter = 0
        while used  and counter < 5:
            counter+=1
            used = Mutex.browser_is_used()
            print("waiting for browser")
            sleep(5)
