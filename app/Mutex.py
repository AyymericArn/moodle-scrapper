from os import path
class Mutex:
    
    
    def browser_is_used() -> bool:
        if path.exists("used.txt"):
            used = open("used.txt")
            content = used.read()
            used.close()
            return "true" in content
        return False
    
    def mark_browser_free() -> None:
        used = open("used.txt", "w")
        used.write("false")
        used.close()
    
    def mark_browser_used() -> None:
        used = open("used.txt", "w")
        used.write("true")
        used.close()