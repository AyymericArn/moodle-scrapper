from time import sleep

from decouple import config
from selenium.webdriver.common.by import By

from Driver import Driver
from Mutex import Mutex

class Cookie:

    @staticmethod    
    def moodle_session() -> str:
        cookie = Cookie()
        return cookie.get_moodle_session()

    def get_moodle_session(self):
        moodle_session = config("MOODLE_SESSION")
        if not moodle_session:
            moodle_session = self.get_moodle_session_with_selenium()
        return moodle_session

    def get_moodle_session_with_selenium(self) -> str:
        self.driver = Driver()
        try:
            self.go_to_login_form()
            self.wait()
            self.fill_login_form()
            moodle_session = self.extract_moodle_session()
        finally:
            self.driver.quit()
            Mutex.mark_browser_free()

        return moodle_session

    def go_to_login_form(self) -> None:
        self.driver.get("https://moodle.myefrei.fr/login/index.php")
        self.driver.find_element(By.CLASS_NAME, "potentialidp").find_element(By.TAG_NAME, "a").click()

    def fill_login_form(self, username: str = config("EFREI_LOGIN"),password: str = config("EFREI_PASSWORD")):
        self.driver.find_element(By.ID, "username").send_keys(username)
        self.driver.find_element(By.ID, "password").send_keys(password)
        self.driver.find_element(By.CLASS_NAME, "submit-button-container").find_element(By.TAG_NAME, "button").click()

    def extract_moodle_session(self) -> str:
        moodle_session_cookie = None
        counter = 0
        while(not moodle_session_cookie and counter <5):
            self.wait()
            counter+=1
            moodle_session_cookie = self.driver.get_cookie("MoodleSession")
        if(moodle_session_cookie):
            return moodle_session_cookie["value"]
        raise Exception("Can't get moodle session")
    
    def wait(self):
        sleep(1)
    